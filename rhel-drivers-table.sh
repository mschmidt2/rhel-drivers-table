#!/bin/bash
# SPDX-FileCopyrightText: ⓒ 2021 Michal Schmidt <mschmidt@redhat.com>, Red Hat Inc.
# SPDX-License-Identifier: MIT


# TODO: handle drivers where the source code moved (e.g. r8169)
# TODO: add wireless
# TODO: autogenerate the 'drivers' array
# TODO: autodetect upstream releases
# TODO: output directly to gdocs

# avoid possible localization surprises in git output
export LC_ALL=C

# the remote names are specific to my setup
rhel=(
	'RHEL 7.9;rhel7/main'
#	'RHEL 8.0;rhel8/8.0'
#	'RHEL 8.1;rhel8/8.1'
#	'RHEL 8.2;rhel8/8.2'
#	'RHEL 8.3;rhel8/8.3'
	'RHEL 8.4;rhel8/8.4'
#	'RHEL 8.5;rhel8/8.5'
	'RHEL 8.6;rhel8/8.6'
#	'RHEL 8.7;rhel8/8.7'
	'RHEL 8.8;rhel8/8.8'
#	'RHEL 8.9;rhel8/8.9'
	'RHEL 8.10;rhel8/main'
	'RHEL 9.0;rhel9/9.0'
#	'RHEL 9.1;rhel9/9.1'
	'RHEL 9.2;rhel9/9.2'
#	'RHEL 9.3;rhel9/9.3'
	'RHEL 9.4;rhel9/9.4'
	'RHEL 9.5;rhel9/9.5'
	'RHEL 9.6 (WIP);rhel9/main'
	'RHEL 10.0 (WIP);rhel10/main'
)

upstream=(
	4.4  4.5  4.6  4.7  4.8  4.9  4.10 4.11
	4.12 4.13 4.14 4.15 4.16 4.17 4.18 4.19
	5.0  5.1  5.2  5.3  5.4  5.5  5.6  5.7
	5.8  5.9  5.10 5.11 5.12 5.13 5.14 5.15
	5.16 5.17 5.18 5.19 6.0  6.1  6.2  6.3
	6.4  6.5  6.6  6.7  6.8  6.9  6.10 6.11
	6.12 6.13
)

drivers=(
	     '8139cp;drivers/net/ethernet/realtek/8139cp.c'
	    '8139too;drivers/net/ethernet/realtek/8139too.c'
	        'alx;drivers/net/ethernet/atheros/alx'
	   'amd-xgbe;drivers/net/ethernet/amd/xgbe'
	      'atl1c;drivers/net/ethernet/atheros/atl1c'
	      'atl1e;drivers/net/ethernet/atheros/atl1e'
	       'atl1;drivers/net/ethernet/atheros/atlx/atl1.c'
	       'atl2;drivers/net/ethernet/atheros/atlx/atl2.c'
	   'atlantic;drivers/net/ethernet/aquantia/atlantic'
	     'be2net;drivers/net/ethernet/emulex/benet'
	       'bnx2;drivers/net/ethernet/broadcom/bnx2.c'
	      'bnx2x;drivers/net/ethernet/broadcom/bnx2x'
	    'bnxt_en;drivers/net/ethernet/broadcom/bnxt'
	   'ch_ipsec;drivers/net/ethernet/chelsio/inline_crypto/ch_ipsec'
	    'ch_ktls;drivers/net/ethernet/chelsio/inline_crypto/ch_ktls'
	       'cnic;drivers/net/ethernet/broadcom/cnic.c'
	      'cxgb4;drivers/net/ethernet/chelsio/cxgb4'
	    'cxgb4vf;drivers/net/ethernet/chelsio/cxgb4vf'
	       'dl2k;drivers/net/ethernet/dlink'
	       'dnet;drivers/net/ethernet/dnet.c'
	      'e1000;drivers/net/ethernet/intel/e1000'
	     'e1000e;drivers/net/ethernet/intel/e1000e'
	        'ena;drivers/net/ethernet/amazon/ena'
	       'enic;drivers/net/ethernet/cisco/enic'
	      'ethoc;drivers/net/ethernet/ethoc.c'
	      'fm10k;drivers/net/ethernet/intel/fm10k'
	        'gve;drivers/net/ethernet/google/gve'
	      'hinic;drivers/net/ethernet/huawei/hinic'
	       'i40e;drivers/net/ethernet/intel/i40e'
	       'iavf;drivers/net/ethernet/intel/iavf'
	        'ice;drivers/net/ethernet/intel/ice'
	       'idpf;drivers/net/ethernet/intel/idpf'
	        'igb;drivers/net/ethernet/intel/igb'
	      'igbvf;drivers/net/ethernet/intel/igbvf'
	        'igc;drivers/net/ethernet/intel/igc'
	      'ionic;drivers/net/ethernet/pensando/ionic'
	       'ixgb;drivers/net/ethernet/intel/ixgb'
	      'ixgbe;drivers/net/ethernet/intel/ixgbe'
	    'ixgbevf;drivers/net/ethernet/intel/ixgbevf'
	   'liquidio;drivers/net/ethernet/cavium/liquidio'
	       'mana;drivers/net/ethernet/microsoft/mana'
	    'mlx4_en;drivers/net/ethernet/mellanox/mlx4'
	  'mlx5_core;drivers/net/ethernet/mellanox/mlx5'
	      'mlxfw;drivers/net/ethernet/mellanox/mlxfw'
	    'mlxsw_*;drivers/net/ethernet/mellanox/mlxsw'
	   'myri10ge;drivers/net/ethernet/myricom/myri10ge'
	 'netxen_nic;drivers/net/ethernet/qlogic/netxen'
	        'nfp;drivers/net/ethernet/netronome/nfp'
	  'octeon_ep;drivers/net/ethernet/marvell/octeon_ep'
	  'octeontx2;drivers/net/ethernet/marvell/octeontx2'
	        'qed;drivers/net/ethernet/qlogic/qed'
	       'qede;drivers/net/ethernet/qlogic/qede'
	    'qla3xxx;drivers/net/ethernet/qlogic/qla3xxx.c'
	      'r8169;drivers/net/ethernet/realtek/r8169*.c'
	        'sfc;drivers/net/ethernet/sfc'
	     'stmmac;drivers/net/ethernet/stmicro/stmmac'
	        'tg3;drivers/net/ethernet/broadcom/tg3.c'

	    'bnxt_re;drivers/infiniband/hw/bnxt_re'
	   'iw_cxgb4;drivers/infiniband/hw/cxgb4'
	        'efa;drivers/infiniband/hw/efa'
	       'hfi1;drivers/infiniband/hw/hfi1'
	      'i40iw;drivers/infiniband/hw/i40iw'
	      'irdma;drivers/infiniband/hw/irdma'
	    'mlx4_ib;drivers/infiniband/hw/mlx4'
	    'mlx5_ib;drivers/infiniband/hw/mlx5'
	       'qedr;drivers/infiniband/hw/qedr'
	'usnic_verbs;drivers/infiniband/hw/usnic'
	 'vmw_pvrdma;drivers/infiniband/hw/vmw_pvrdma'
)

# header row
printf ';'
for rb in "${rhel[@]}"; do
	r="${rb%;*}"
	printf '%s; ' "$r"
done
printf '\n'

# driver; version; version; ...
for dp in "${drivers[@]}"; do
	d="${dp%;*}"
	p="${dp#*;}"

	printf '%s; ' "$d"
	for rb in "${rhel[@]}"; do
		branch="${rb#*;}"

		if [ -z "$(git ls-tree $branch -- $p)" ]; then
			printf ' - ; '
			continue
		fi

		closest_upstream=''
		closest_diffstat=999999999
		for u in ${upstream[@]}; do
			diffstat_line=$(git diff --shortstat $branch v$u -- $p)
			if [ $? != 0 ]; then
				echo 'git diff FAILED!'
				exit 1
			fi
			if [ -z "$diffstat_line" ]; then
				# empty diff, perfect match
				closest_upstream=$u
				closest_diffstat=0
			else
				insertions=0
				deletions=0
				[[ "$diffstat_line" =~ \ ([0-9]+)\ insertion ]] && insertions=${BASH_REMATCH[1]}
				[[ "$diffstat_line" =~ \ ([0-9]+)\ deletion  ]] &&  deletions=${BASH_REMATCH[1]}
				total_lines=$(( $insertions + $deletions ))

				if [ $total_lines -eq 0 ]; then
					echo 'Zero insertions and deletions. Sad!'
					exit 1
				fi

				if [ $total_lines -le $closest_diffstat ]; then
					closest_upstream=$u
					closest_diffstat=$total_lines
				fi
			fi
		done

		printf '%s; ' $closest_upstream
	done
	printf '\n'
done
